package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.service.*;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class TablesModificationController {
    private Model model;

    public TablesModificationController() {
        model = SessionModel.getModel();
    }

    @RequestMapping("/modify")
    public String user(@RequestParam String command, WebRequest webRequest) throws IOException, ClientException {
        if (command.equals("modifyActivityUser")) return new ActivityUsersModify().execute(model, webRequest);
        if (command.equals("modifyCategories")) return new CategoryModifyService().execute(model, webRequest);
        if (command.equals("modifyUsers")) return new UsersModify().execute(model, webRequest);
        if (command.equals("modifyActivities")) return new ActivitiesModify().execute(model, webRequest);
        if (command.equals("modifyActivitiesLocalization"))
            return new ActivitiesLocalizationModify().execute(model, webRequest);
        if (command.equals("modifyCategoriesLocalization"))
            return new CategoryLocalizationModifyService().execute(model, webRequest);
        if (command.equals("sortActivityByName")) return new SortService().execute(model, webRequest);
        if (command.equals("sortActivityByCategory")) return new SortService().execute(model, webRequest);
        if (command.equals("sortActivityByAmount")) return new SortService().execute(model, webRequest);
        if (command.equals("usersReport")) return new UsersReportService().execute(model, webRequest);
        if (command.equals("detail")) return new UsersReportDetailService().execute(model, webRequest);
        return null;
    }
}
