package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.CategoryDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.entity.Category;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class CategoryLocalizationModifyService {
    private static final Logger LOG = Logger.getLogger(CategoryLocalizationModifyService.class);


    public String execute(Model model, WebRequest request) throws IOException {
        String combobox = request.getParameter("combobox_localization");
        LOG.trace("Request parameter: combobox_localization -->" + combobox);

        // register a new user from the request
        String category_name = request.getParameter("category_name_localization");
        LOG.trace("Request parameter: activity_id --> " + category_name);

        String translation = request.getParameter("translation");
        LOG.trace("Request parameter: translation --> " + translation);

        String radio = request.getParameter("language");
        LOG.trace("Request parameter: radio--> " + radio);


        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        String language = null;

        if (category_name == null || translation == null) {
            errorMessage = "Parameters cannot be empty";
            request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }

        Category category = Category.createCategory(category_name);
        category.setNameTranslation(translation);
        //insert the translation of category
        if (combobox.equals("0")) {
            new CategoryDAO().insertCategory(category, "English");
        }
        //update the translation of category
        if (combobox.equals("1")) {
            LOG.trace("Update category translation");
            new CategoryDAO().updateCategory(category, "English");

        }
        //delet the translation
        if (combobox.equals("2")) {
            new CategoryDAO().deleteCategory(category, "English");
        }


        return "redirect:" + Path.COMMAND__CATEGORIES;
    }
}
