package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.RequestDAO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class DeleteUserActivityService {
    private static final Logger LOG = Logger.getLogger(DeleteUserActivityService.class);
    private static final Integer STATUS = 3;


    public String execute(Model model, WebRequest request) throws IOException {
        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);
        Long userId = (Long) model.getAttribute("userId");
        LOG.trace("Id -- >" + userId);

        String activityId = request.getParameter("activityId");
        LOG.trace("Activity ID-->" + activityId);

        LOG.trace("Changing the activity status");
        //change the record status to ("waiting for deleting") in activities_users table
        new RequestDAO().update(userId, Long.parseLong(activityId), STATUS);

        return "redirect:" + Path.COMMAND__LIST_USERS_ACTIVITIES;
    }
}
