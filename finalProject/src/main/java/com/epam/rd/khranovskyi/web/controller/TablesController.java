package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.service.ListOfActivitiesService;
import com.epam.rd.khranovskyi.service.ListOfCategoriesService;
import com.epam.rd.khranovskyi.service.ListOfUsersService;
import com.epam.rd.khranovskyi.service.ListUsersService;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class TablesController {
   private Model model;

    public TablesController() {
        model = SessionModel.getModel();
    }

    @RequestMapping("/table")
    public String user(@RequestParam String command, WebRequest webRequest) throws IOException, ClientException {
        if (command.equals("usersTable")) return new ListOfUsersService().execute(model, webRequest);
        if (command.equals("activitiesTable")) return new ListOfActivitiesService().execute(model, webRequest);
        if (command.equals("categoriesTable")) return new ListOfCategoriesService().execute(model, webRequest);
        if (command.equals("listUsers")) return new ListUsersService().execute(model, webRequest);
        return null;
    }
}
