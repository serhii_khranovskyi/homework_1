package com.epam.rd.khranovskyi.dao;

public class Fields {

    public static final String USER__ID = "id_users";

    public static final String USER__MAIL = "mail";
    public static final String USER__PASSWORD = "password";
    public static final String USER__FIRST_NAME = "first_name";
    public static final String USER__LAST_NAME = "last_name";

    public static final String ACTIVITY_ID = "id_activities";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String ACTIVITY__CATEGORY_ID = "categories_id_categories";


    public static final String CATEGORIES__ID_CATEGORIES = "id_categories";
    public static final String CATEGORIES__CATEGORY_NAME = "category_name";

    public static final String ACTIVITIES_USERS__ACTIVITIES_ID_ACTIVITIES = "activities_id_activities";
    public static final String ACTIVITIES_USERS__USERS_ID_USERS = "users_id_users";
    public static final String ACTIVITIES_USERS__BEGINNING = "beginning";
    public static final String ACTIVITIES_USERS__ENDING = "ending";
    public static final String ACTIVITIES_USERS__STATUS = "status";

    public static final String ACTIVITY_TRANSLATION = "translation";
    public static final String CATEGORY_TRANSLATION = "translation";
}
