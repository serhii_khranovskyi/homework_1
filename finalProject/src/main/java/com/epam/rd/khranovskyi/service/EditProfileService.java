package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.EmailSender;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import com.epam.rd.khranovskyi.web.annotation.Time;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class EditProfileService {
    private static final Logger LOG = Logger.getLogger(EditProfileService.class);

    @Time
    public String execute(Model model, WebRequest request) throws IOException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        LOG.info("Execution starts !!!!");
        try {

            String language = (String) model.getAttribute("defaultLocale");
            String mail = request.getParameter("mail");
            LOG.trace("Request parameter: mail -->" + mail);

            // Get parameters from the request
            String first_name = request.getParameter("first_name");
            LOG.trace("Request parameter: first_name --> " + first_name);

            String last_name = request.getParameter("last_name");
            LOG.trace("Request parameter: last_name --> " + last_name);

            String password = request.getParameter("password");
            LOG.trace("Request parameter: password --> " + password);

            String password1 = request.getParameter("password1");
            LOG.trace("Request parameter: password1 --> " + password1);

            Long userId = (Long) model.getAttribute("userId");

            String errorMessage = null;
            String forward = Path.PAGE__ERROR_PAGE;

            if (mail == null || first_name == null || last_name == null || password == null || password1 == null) {
                errorMessage = "Parameters cannot be empty";
                request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
                LOG.error("errorMessage --> " + errorMessage);
                return forward;
            }
            if (!password.equals(password1)) {
                errorMessage = "Passwords do not match";
                request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
                LOG.error("errorMessage --> " + errorMessage);
                return forward;
            }
            User user = User.createUser(mail, first_name, last_name, password, Role.USER, Language.ENGLISH, new ArrayList<Activity>());
            user.setId(userId);
            try {
                //Updating the user profile
                new UserDAO().updateUserProfile(user);
                //Sending email
                EmailSender.sendMail(mail, user, language);
            } catch (Exception ex) {
                LOG.trace(ex);
            }
        } finally {
            stopWatch.stop();
            LOG.info("Execution took -->" + stopWatch.getLastTaskTimeMillis());
        }
        return "redirect:" + Path.COMMAND__PROFILE;
    }
}
