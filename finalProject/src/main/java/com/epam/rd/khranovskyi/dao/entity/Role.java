package com.epam.rd.khranovskyi.dao.entity;

public enum Role {
    USER(0),
    ADMIN(1);
    private int code;

    public void setCode(int code) {
        this.code = code;
    }

    public static Role get(int roleID) {
        if (USER.getCode() == roleID) return USER;
        return ADMIN;
    }

    Role(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name().toLowerCase();
    }

}
