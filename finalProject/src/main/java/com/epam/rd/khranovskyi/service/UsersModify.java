package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class UsersModify {
    private static final Logger LOG = Logger.getLogger(UsersModify.class);

    public String execute(Model model, WebRequest request) throws IOException {
        String combobox = request.getParameter("combobox");
        LOG.trace("Request parameter: combobox -->" + combobox);

        //Get parameters from the request
        String first_name = request.getParameter("first_name");
        LOG.trace("Request parameter: first_name --> " + first_name);


        String last_name = request.getParameter("last_name");
        LOG.trace("Request parameter: last_name --> " + last_name);

        String mail = request.getParameter("mail");
        LOG.trace("Request parameter: mail --> " + mail);

        String password = request.getParameter("password");
        LOG.trace("Request parameter: password --> " + password);

        String selectEnglish = request.getParameter("selectEnglish");
        LOG.trace("Request parameter: selectEnglish=--> " + selectEnglish);

        String selectRussian = request.getParameter("selectRussian");
        LOG.trace("Request parameter: selectRussian--> " + selectRussian);

        String selectUser = request.getParameter("selectUser");
        LOG.trace("Request parameter: selectUser--> " + selectUser);
        String selectAdmin = request.getParameter("selectAdmin");
        LOG.trace("Request parameter: selectAdmin--> " + selectAdmin);


        // error handler
        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (first_name == null || last_name == null ||
                mail == null || password == null ||
                !(selectRussian == null || selectEnglish == null) || !(selectUser == null || selectAdmin == null)) {
            errorMessage = "Parameters cannot be empty";
            request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }

        //Get Role
        Role role = null;
        if (selectUser == null) role = Role.ADMIN;
        else if (selectAdmin == null) {
            role = Role.USER;
        }
        LOG.info("Set role-->" + role.getName());
        Language language = null;
        //Get choosing language
        if (selectEnglish == null) language = Language.RUSSIAN;
        else if (selectRussian == null) language = Language.ENGLISH;

        ArrayList<Activity> activities = new ArrayList<>();
        LOG.info("Set language-->" + language.getName());
        User user = User.createUser(mail, first_name, last_name, password, role, language, activities);
        LOG.info("Create user");
        //Insert user
        if (combobox.equals("0")) {
            LOG.trace("method insert ");
            new UserDAO().insertUser(user);
        }
        //Update user
        if (combobox.equals("1")) {
            LOG.trace("method update");
            new UserDAO().updateUser(user);

        }
        //Delete user
        if (combobox.equals("2")) {
            LOG.trace("method delete");
            new UserDAO().deleteUser(mail);

        }

        return "redirect:" + Path.COMMAND__USERS;
    }
}
