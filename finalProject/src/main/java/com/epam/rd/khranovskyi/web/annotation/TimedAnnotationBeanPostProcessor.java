package com.epam.rd.khranovskyi.web.annotation;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;


@Component
public class TimedAnnotationBeanPostProcessor implements BeanPostProcessor {
    private static final Logger LOG = Logger.getLogger(TimedAnnotationBeanPostProcessor.class);

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (hasAnnotation(bean)) {
            return proxy(bean);
        }
        return bean;
    }

    private Object proxy(Object bean) {
        ProxyFactory proxyFactory = new ProxyFactory(bean);
        proxyFactory.addAdvice(new TimeMethodInterceptor());
        return proxyFactory.getProxy();
    }

    private boolean hasAnnotation(Object bean) {
        Method[] methods = ReflectionUtils.getAllDeclaredMethods(bean.getClass());

        for (Method method : methods) {
            if (method.isAnnotationPresent(Time.class)) {
                return true;
            }
        }

        return false;
    }

    private static class TimeMethodInterceptor implements MethodInterceptor {
        @Override
        public Object invoke(MethodInvocation invocation) throws Throwable {
            final Method method = invocation.getMethod();
            final Time annotation = AnnotationUtils.getAnnotation(method, Time.class);
            return annotation == null
                    ? invocation.proceed()
                    : proceed(invocation);
        }

        private Object proceed(MethodInvocation invocation) throws Throwable {
            final StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            try {
                return invocation.proceed();
            } finally {
                stopWatch.stop();
                LOG.warn(
                        "Execution of " + resolveLogMethod(invocation) + " took " + stopWatch.getTotalTimeMillis() + " ms");
            }
        }

        private String resolveLogMethod(MethodInvocation invocation) {
            return invocation.getMethod().getDeclaringClass().getCanonicalName() + "#" + invocation.getMethod().getName();
        }
    }
}
