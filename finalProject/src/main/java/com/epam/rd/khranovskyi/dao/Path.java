package com.epam.rd.khranovskyi.dao;

public class Path {
    // pages
    public static final String PAGE__LOGIN = "/login";
    public static final String PAGE__ERROR_PAGE = "/jsp/error_page";
    public static final String PAGE__LIST_USERS = "/jsp/admin/list_users";
    public static final String PAGE__USER_REPORT = "/jsp/admin/users_report";
    public static final String PAGE__USER_REPORT_DETAIL = "/jsp/admin/users_report_detail";
    public static final String PAGE__USERS = "/jsp/admin/users";
    public static final String PAGE__ACTIVITIES = "/jsp/admin/activities";
    public static final String PAGE__LIST_CATEGORIES = "/jsp/user/users_main_page";
    public static final String PAGE__LIST_USERS_ACTIVITIES = "/jsp/user/users_activities";
    public static final String PAGE__LIST_USERS_ACTIVITY_HISTORY = "/jsp/user/users_activity_history";
    public static final String PAGE__LIST_ACTIVITIES = "/jsp/user/list_activities";
    public static final String PAGE__CATEGORIES = "/jsp/admin/categories";
    public static final String PAGE__PROFILE = "/jsp/user/profile";

    // commands
    public static final String COMMAND__LIST_USERS = "/table?command=listUsers";
    public static final String COMMAND__USERS = "/table?command=usersTable";
    public static final String COMMAND__ACTIVITIES = "/table?command=activitiesTable";
    public static final String COMMAND__CATEGORIES = "/table?command=categoriesTable";

    //user commands
    public static final String COMMAND__LIST_USERS_ACTIVITIES = "/user?command=myActivitiesCommand";
    public static final String COMMAND__SELECTED_CATEGORY = "/user?command=selectedCategory";
    public static final String COMMAND__PROFILE = "/user?command=profile";
}
