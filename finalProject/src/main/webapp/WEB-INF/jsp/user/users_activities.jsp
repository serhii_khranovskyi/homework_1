<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List of activities" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>

<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 550px"><a href="/accounting_time/user?command=profile"><fmt:message
                key="users_activities_jsp.head.link.profile"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/user?command=users_main_page"><fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>

<h3><fmt:message key="users_activities_jsp.h3.header.activities"/></h3>


<c:choose>
    <c:when test="${fn:length(userActivityBeans) == 0}">No such table</c:when>

    <c:otherwise>

        <c:forEach var="bean" items="${userActivityBeans}" varStatus="status">

            <p>${bean.name}</p>
            <form action="user" method="post" id="${status.index}">
                <input name="command" type="hidden" value="updateUserActivity"/>
                <input name="activityId" type="hidden" value="${bean.id}"/>
                <input type="submit" value="<fmt:message key="users_activities_jsp.body.button.submit"/>"/>
            </form>
            <form action="user" method="post" id="${status.index}">
                <input name="command" type="hidden" value="deleteUserActivity"/>
                <input name="activityId" type="hidden" value="${bean.id}"/>
                <input type="submit" value="<fmt:message key="users_activities_jsp.body.button.delete"/>"/>
            </form>
            <form action="user" method="post" id="${status.index}">
                <input name="command" type="hidden" value="historyUserActivity"/>
                <input name="activityId" type="hidden" value="${bean.id}"/>
                <input type="submit" value="<fmt:message key="users_activities_jsp.body.button.info"/>"/>
            </form>
            <c:choose>
                <c:when test="${fn:contains(bean.begin,'1999-12-31 23:00:00.0')}"><fmt:message
                        key="users_activities_jsp.body.message"/></c:when>
                <c:otherwise>
                    <p><fmt:message key="users_activities_jsp.body.message.started"/>${bean.begin}</p>
                    <c:choose>
                        <c:when test="${fn:contains(bean.end,'1999-12-31 23:00:00.0')}"><fmt:message
                                key="users_activities_jsp.body.message_end"/></c:when>
                        <c:otherwise>
                            <p><fmt:message key="users_activities_jsp.body.message.ended"/>${bean.end}</p>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
            </form>
        </c:forEach>

    </c:otherwise>
</c:choose>


<table style="width: 90%; margin: auto">
    <tr>
        <c:if test="${currentPage != 1}">

            <form action="user" method="post">
                <input type="hidden" name="command" value="myActivitiesCommand">
                <input type="hidden" name="page" value="${currentPage - 1}">
                <input type="submit" value="Previous">
            </form>

        </c:if>


        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <input type="submit" value="${i}">
                </c:when>
                <c:otherwise>

                    <form action="user" method="post">
                        <input type="hidden" name="command" value="myActivitiesCommand">
                        <input type="hidden" name="page" value="${i}">
                        <input type="submit" value="${i}">
                    </form>

                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:if test="${currentPage lt noOfPages}">
            <form action="user" method="post">
                <input type="hidden" name="command" value="myActivitiesCommand">
                <input type="hidden" name="page" value="${currentPage + 1}">
                <input type="submit" value="Next">
            </form>
        </c:if>
    </tr>
</table>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
