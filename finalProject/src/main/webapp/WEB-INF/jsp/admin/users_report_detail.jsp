/accounting_time/logout
<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List users" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>

<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a href="/accounting_time/modify?command=usersReport"><fmt:message
                key="users_jsp.head.link.report"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/table?command=usersTable"><fmt:message
                key="list_users_jsp.table.header.user"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>
<table id="main-container">

    <tr>
        <td class="insert update delete activity_user">
            <c:choose>
                <c:when test="${fn:length(activities) == 0}">No such table</c:when>

                <c:otherwise>
                    <table id="list_order_table" class="data">
                        <thead class="data">
                        <tr>
                            <td><fmt:message key="activities_jsp.table.header.activity_name"/></td>
                            <td><fmt:message key="users_jsp.table.header.amountOfTime"/></td>
                        </tr>
                        </thead>
                        <tbody class="data">
                        <c:forEach var="bean" items="${activities}">

                            <tr>
                                <td>${bean.name}</td>
                                <td>${bean.allTime}</td>
                            </tr>

                        </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>

        </td>
    </tr>

</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>