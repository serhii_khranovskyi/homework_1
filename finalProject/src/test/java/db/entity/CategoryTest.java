package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Category;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryTest {
    @Test
    void getTranslation(){
        Category category = new Category();
        category.setNameTranslation("test");
        assertEquals("test",category.getNameTranslation());
    }

    @Test
    void setTranslation(){
        Category category = new Category();
        category.setNameTranslation("test");
        assertEquals("test",category.getNameTranslation());
    }

    @Test
    void getLocale(){
        Category category = new Category();
        category.setLocale(1);
        assertEquals(1,category.getLocale());
    }

    @Test
    void setLocale(){
        Category category = new Category();
        category.setLocale(1);
        assertEquals(1,category.getLocale());
    }

    @Test
    void setNameTest(){
        Category category = new Category();
        category.setName("Test");
        assertEquals("Test",category.getName());
    }

    @Test
    void getNameTest(){
        Category category = new Category();
        category.setName("Test");
        assertEquals("Test",category.getName());
    }

    @Test
    void categoryToString(){
        Category category = new Category("test");

        assertEquals("Category [name=" + "test" +']',category.toString());
    }

    @Test
    void createCategory(){
        Category category = Category.createCategory("test");
        assertEquals("test",category.getName());
    }
}
