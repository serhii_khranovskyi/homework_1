package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Role;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoleTest {

    @Test
    void get(){
        assertEquals(Role.USER, Role.get(0));
    }

    @Test
    void getAdmin(){
        assertEquals(Role.ADMIN, Role.get(1));
    }

    @Test
    void getCode(){
        assertEquals(0,Role.USER.getCode());
    }

    @Test
    void getName(){
        assertEquals("user",Role.USER.getName());
    }
}
