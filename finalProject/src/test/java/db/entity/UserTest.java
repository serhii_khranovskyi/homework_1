package db.entity;

import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {

    @Test
    void createUser(){
        User user = User.createUser("mail@mail.com","test","test","123", Role.USER, Language.ENGLISH,new ArrayList<Activity>());
        assertEquals( "User [ mail = mail@mail.com; first_name = "+
                "test; last_name = test; password = 123; language = "+
                "english; role = user",user.toString());
    }

    @Test
    void setActivitiesCount(){
        User user = new User();
        user.setActivitiesCount(2);
        assertEquals(2,user.getActivitiesCount());
    }

    @Test
    void setAllActivitiesTime(){
        User user = new User();
        user.setAllActivitiesTime(10);
        assertEquals(10,user.getAllActivitiesTime());
    }

    @Test
    void getLanguage(){
        User user = new User();
        user.setLanguage(Language.ENGLISH);
        assertEquals(Language.ENGLISH, user.getLanguage());
    }

    @Test
    void getMail(){
        User user = new User();
        user.setMail("test@mail.com");
        assertEquals("test@mail.com", user.getMail());
    }

    @Test
    void getFirst_name(){
        User user = new User();
        user.setFirst_name("test");
        assertEquals("test", user.getFirst_name());
    }

    @Test
    void getLast_name(){
        User user = new User();
        user.setLast_name("test");
        assertEquals("test", user.getLast_name());
    }

    @Test
    void getPassword(){
        User user = new User();
        user.setPassword("test");
        assertEquals("test", user.getPassword());
    }

    @Test
    void getRole(){
        User user = new User();
        user.setRole(Role.USER);
        assertEquals(Role.USER, user.getRole());
    }

    @Test
    void getActivities(){
        User user = new User();

        ArrayList<Activity> activities = new ArrayList<>();
        Activity activity = new Activity();

        activity.setName("test");
        activities.add(activity);

        user.setActivities(activities);
        assertEquals(activities, user.getActivities());
    }
}
