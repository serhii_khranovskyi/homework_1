package db.bean;
import com.epam.rd.khranovskyi.dao.bean.CategoryBean;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class CategoryBeanTest {
    @Test
    void getTranslation() {
        CategoryBean bean = new CategoryBean();
        bean.setTranslation("test");
        assertEquals("test",bean.getTranslation());
    }

    @Test
    void getName() {
        CategoryBean bean = new CategoryBean();
        bean.setName("test");
        assertEquals("test",bean.getName());
    }

    @Test
    void setTranslation() {
        CategoryBean bean = new CategoryBean();
        bean.setTranslation("test");
        assertEquals("test",bean.getTranslation());
    }

    @Test
    void setName() {
        CategoryBean bean = new CategoryBean();
        bean.setTranslation("test");
        assertEquals("test",bean.getTranslation());
    }

    @Test
    void beanToString() {
        CategoryBean bean = new CategoryBean();
        bean.setName("test");
        assertEquals("CategoryBean{" +
            "name='" + "test"+ '\'' +
                    '}',bean.toString());
    }
}
