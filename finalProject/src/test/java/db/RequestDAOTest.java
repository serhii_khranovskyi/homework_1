package db;

import com.epam.rd.khranovskyi.dao.RequestDAO;
import com.epam.rd.khranovskyi.dao.entity.Request;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RequestDAOTest {

   @Test
    void Test() {
        Request result = new RequestDAO().getUsersActivityId(2, 6);
        assertEquals("2020-10-01 01:01:00", result.getStart().toString());
        assertEquals("2020-10-02 02:02:00", result.getEnd().toString());
        assertEquals(16, result.getId());
    }
}
